package com.example.proximitychat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.InetAddresses;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button btnDiscover, btnOnOff, btnSend;
    ListView listView;
    TextView read_msg_box, connectionsStatus;
    EditText writeMsg;

    WifiManager wifiManager;

    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;

    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;

    List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();
    String[] deviceNameArray;
    WifiP2pDevice[] deviceArray;

    static final int MESSAGE_READ = 1;

    ServerClass serverClass;
    ClientClass clientClass;
    SendReceive sendReceive;

    String tempMsg;

    String whatAmI;

    private InputStream inputStream;
    private OutputStream outputStream;

    private byte[] generalBytes;

    LinearLayout layout1, layout2;

    int oldId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialWork();
        exqListener();

        btnDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setVisibility(View.VISIBLE);
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(
                            MainActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1
                    );
                    return;
                }
                mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        connectionsStatus.setText("Discovery started");
                    }

                    @Override
                    public void onFailure(int reason) {
                        connectionsStatus.setText("Discovery started failed");
                    }
                });

            }
        });

    }

    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch (msg.what){
                case MESSAGE_READ:
                    byte[] readBuff = (byte[]) msg.obj;
                    tempMsg = new String(readBuff, 0, msg.arg1);
                    read_msg_box.setText(tempMsg);
                    String tempWhatAmI;
                    if(whatAmI.equalsIgnoreCase("host")){
                        tempWhatAmI = "client";
                    }else{
                        tempWhatAmI = "host";
                    }
                    printMessage(new MessageType(tempMsg, tempWhatAmI));
                    break;
            }
            return true;
        }
    });

    public void showInput(){
        writeMsg.setVisibility(View.VISIBLE);
        btnSend.setVisibility(View.VISIBLE);
    }

    public void hideMainScreenComponents(){
        btnDiscover.setVisibility(View.GONE);
        read_msg_box.setVisibility(View.GONE);
        connectionsStatus.setVisibility(View.GONE);
    }

    public void printMessage(MessageType message){

        RelativeLayout relativeLayout = findViewById(R.id.chat_layout);

        TextView textView = new TextView(this);
        textView.setText(message.getText());

        textView.setPadding(30, 20, 30, 20);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        textView.setBackground(getResources().getDrawable(R.drawable.radius));

        //textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        //textView.setPadding(30, 20, 30, 20);

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        //lp.addRule(RelativeLayout.CENTER_IN_PARENT);

        if(message.getAuthor().equalsIgnoreCase("host")){

            Drawable oldBack = textView.getBackground();

            if(whatAmI.equalsIgnoreCase("host")){
                ((GradientDrawable)oldBack).setColor(ContextCompat.getColor(MainActivity.this, R.color.azul_claro1));
                lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                lp.setMargins(70, 20, 25,0);
            }else if(whatAmI.equalsIgnoreCase("client")){
                ((GradientDrawable)oldBack).setColor(ContextCompat.getColor(MainActivity.this, R.color.azul_oscuro1));
                lp.setMargins(25, 20, 70,0);
            }

            Log.i("Holi", message.getAuthor());


        } else { // Si el mensaje es del client

            Drawable oldBack = textView.getBackground();

            if(whatAmI.equalsIgnoreCase("client")){
                ((GradientDrawable)oldBack).setColor(ContextCompat.getColor(MainActivity.this, R.color.azul_claro1));
                lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                lp.setMargins(70, 20, 25,0);
            }else if(whatAmI.equalsIgnoreCase("host")){
                ((GradientDrawable)oldBack).setColor(ContextCompat.getColor(MainActivity.this, R.color.azul_oscuro1));
                lp.setMargins(25, 20, 70,0);
            }

            Log.i("Holi", message.getAuthor());
        }

        textView.setTextColor(getResources().getColor(R.color.gris_claro));

        int newId = View.generateViewId();

        textView.setId(newId);

        if(oldId != -1){
            lp.addRule(RelativeLayout.BELOW, oldId);
        }

        oldId = newId;

        textView.setLayoutParams(lp);

        relativeLayout.addView(textView);

        //linearLayout.setOrientation(LinearLayout.VERTICAL);

    }

    public void exqListener() {

        btnOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wifiManager.isWifiEnabled()) {
                    wifiManager.setWifiEnabled(false);
                } else {
                    wifiManager.setWifiEnabled(true);
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                final WifiP2pDevice device = deviceArray[i];
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = device.deviceAddress;

                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions(
                            MainActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1
                    );
                }
                mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(MainActivity.this, "Connected to " + device.deviceName , Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reason) {
                        Toast.makeText(MainActivity.this, "Not connected", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = writeMsg.getText().toString();

                if(!msg.equalsIgnoreCase("")){
                    writeMsg.setText("");
                    hideKeyboard(MainActivity.this);
                    sendReceive.write(msg.getBytes());
                    MessageType message = new MessageType(msg, whatAmI);
                    printMessage(message);
                }
            }
        });

    }

    public void initialWork(){

        btnDiscover = findViewById(R.id.discover);
        btnOnOff = findViewById(R.id.onOff);
        btnSend = findViewById(R.id.sendButton);

        writeMsg = findViewById(R.id.writeMsg);

        listView = findViewById(R.id.peerListView);

        read_msg_box = findViewById(R.id.readMsg);
        connectionsStatus = findViewById(R.id.connectionStatus);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);

        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
        mIntentFilter = new IntentFilter();

        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        writeMsg.setVisibility(View.GONE);
        btnSend.setVisibility(View.GONE);

        btnOnOff.setVisibility(View.GONE);

    }

    WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {
            if(!peerList.getDeviceList().equals(peers)){
                peers.clear();
                peers.addAll(peerList.getDeviceList());

                deviceNameArray = new String[peerList.getDeviceList().size()];
                deviceArray = new WifiP2pDevice[peerList.getDeviceList().size()];
                int index = 0;

                for(WifiP2pDevice device: peerList.getDeviceList()){
                    deviceNameArray[index] = device.deviceName;
                    deviceArray[index] = device;
                    index++;
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, deviceNameArray);
                listView.setAdapter(adapter);

            }

            if(peers.size() == 0){
                Toast.makeText(MainActivity.this, "No devices found", Toast.LENGTH_SHORT).show();
            }

        }
    };

    WifiP2pManager.ConnectionInfoListener connectionInfoListener= new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo info) {
            final InetAddress groupOwnerAddress = info.groupOwnerAddress;

            if(info.groupFormed && info.isGroupOwner) {
                connectionsStatus.setText("Host");
                serverClass = new ServerClass();
                serverClass.start();
                listView.setVisibility(View.GONE);
                //Toast.makeText(MainActivity.this, "Eres el host", Toast.LENGTH_SHORT).show();
                whatAmI = "host";
                showInput();
                hideMainScreenComponents();
                changeVisibilityLayouts();
            }else if(info.groupFormed){
                connectionsStatus.setText("Client");
                clientClass = new ClientClass(groupOwnerAddress);
                clientClass.start();
                listView.setVisibility(View.GONE);
                //Toast.makeText(MainActivity.this, "Eres el client", Toast.LENGTH_SHORT).show();
                whatAmI = "client";
                showInput();
                hideMainScreenComponents();
                changeVisibilityLayouts();
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(MainActivity.this, "Unregister", Toast.LENGTH_SHORT).show();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(MainActivity.this, "Register", Toast.LENGTH_SHORT).show();
        registerReceiver(mReceiver, mIntentFilter);
    }

    public void changeVisibilityLayouts(){
        layout1 = findViewById(R.id.firstLayout);
        layout1.setVisibility(View.GONE);
        layout2 = findViewById(R.id.secondLayout);
        layout2.setVisibility(View.VISIBLE);
    }

    public class ServerClass extends Thread {
        Socket socket;
        ServerSocket serverSocket;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(8888);
                socket = serverSocket.accept();
                sendReceive = new SendReceive(socket);
                sendReceive.start();
            } catch (IOException e) {
            }

        }
    }

    public class SendReceive extends Thread {
        private Socket socket;

        public SendReceive(Socket skt){
            socket = skt;
            try {
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();
            } catch (IOException e) {
            }
        }

        @Override
        public void run() {
            byte[] buffer = new byte[1024];
            int bytes;

            while(socket != null){
                try {
                    bytes = inputStream.read(buffer);
                    if(bytes > 0){
                        handler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                    }
                } catch (IOException e) {
                }
            }
        }

        public void write(byte[] bytes){
            try {



                generalBytes = bytes;
                Log.i("Logo: ", "1");
                //outputStream.write(bytes);
                SendMessage sendMessage = new SendMessage();
                sendMessage.start();
                Log.i("Logo: ", "2");
            } catch (Exception e) {
            }
        }

    }

    public class ClientClass extends Thread {

        Socket socket;
        String hostAdd;

        public ClientClass (InetAddress hostAddress) {
            hostAdd = hostAddress.getHostAddress();
            socket = new Socket();
        }

        @Override
        public void run() {
            try {
                socket.connect(new InetSocketAddress(hostAdd, 8888), 1000);
                sendReceive = new SendReceive(socket);
                sendReceive.start();
            } catch (IOException e) {
            }
        }
    }

    public class SendMessage extends  Thread {
        @Override
        public void run() {

            try {
                outputStream.write(generalBytes);
            } catch (IOException e) {
            }

        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}